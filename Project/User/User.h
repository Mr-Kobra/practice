#pragma once
#include <string>
#include <iostream>
using namespace std;

class User {
public:
	User();
	User(string username, string password, string name, string position, string dateRegister);
	User(const User& user);
	User(string dataString);
	template <typename T>
	T GetData(string fieldName);
	template <typename T>
	void SetData(string fieldName, T metaData);
	void Set(string username, string password, string name, string position, int level, string dateRegister);
	string GetDataString();
	friend ostream& operator<< (ostream& out, const User& user);
private:
	unsigned long int _ID;
	string _username;
	string _password;
	string _name;
	string _position;
	int _level;
	string _dateRegister;
	void InitUser();
};