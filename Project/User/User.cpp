#include "User.h"
#include "../Global/Global.h"
#include <vector>
#include "../DataBase/DataBase.h"
using namespace std;

void User::InitUser()
{
	_ID = 0;
	_username = string();
	_password = string();
	_name = string();
	_position = string();
	_dateRegister = string();
	_level = 0;
}

User::User()
{
	InitUser();
}

User::User(string username, string password, string name, string position, string dateRegister)
{
	InitUser();
	DataBase<User> users(userFile);
	if (users.Size() == 0) {
		_ID = 1;
	}
	else {
		_ID = users.GetMaxID() + 1;
	}
	_username = username;
	_password = password;
	_name = name;
	_position = position;
	_dateRegister = dateRegister;
}

User::User(string dataString)
{
	InitUser();
	vector<string> result = Splite(dataString, ';');
	if (result.size() == 7) {
		_ID = atoi(result[0].c_str());
		_username = result[1];
		_password = result[2];
		_name = result[3];
		_position = result[4];
		_level = atoi(result[5].c_str());
		_dateRegister = result[6];
	}
}

User::User(const User& user)
{
	InitUser();
	DataBase<User> users(string("DataBase/Files/Users.txt"));
	_ID = users.GetMaxID() + 1;
	_username = user._username;
	_password = user._password;
	_name = user._name;
	_position = user._position;
	_dateRegister = user._dateRegister;
}

template <>
string User::GetData(string fieldName)
{
	string result{};
	if (fieldName == "username") {
		result = _username;
	}
	else if (fieldName == "password") {
		result = _password;
	}
	else if (fieldName == "name") {
		result = _name;
	}
	else if (fieldName == "position") {
		result = _position;
	}
	else if (fieldName == "date register") {
		result = _dateRegister;
	}
	return result;
}

template <>
unsigned long int User::GetData(string fieldName)
{
	unsigned long int result = 0;
	if (fieldName == "ID") {
		result = _ID;
	}
	return result;
}

template<>
int User::GetData(string fieldName)
{
	int result = -1;
	if (fieldName == "level") {
		result = _level;
	}
	return result;
}

template<>
void User::SetData(string fieldName, string metaData)
{
	if (fieldName == "username") {
		_username = metaData;
	}
	else if (fieldName == "password") {
		_password = metaData;
	}
	else if (fieldName == "name") {
		_name = metaData;
	}
	else if (fieldName == "position") {
		_position = metaData;
	}
	else if (fieldName == "date register") {
		_dateRegister = metaData;
	}
}

template<>
void User::SetData(string fieldName, int metaData)
{
	if (fieldName == "level") {
		_level = metaData;
	}
}

string User::GetDataString()
{
	string result{};
	char* ID = new char[256]{}, * level = new char[256]{};
	sprintf_s(ID, 256, "%d", _ID);
	sprintf_s(level, 256, "%d", _level);
	result = result + ID + ';';
	result = result + _username + ';';
	result = result + _password + ';';
	result = result + _name + ';';
	result = result + _position + ';';
	result = result + level + ';';
	result = result + _dateRegister;
	return result;
}

ostream& operator<< (ostream& out, const User& user)
{
	return out << user._name;
}

void User::Set(string username, string password, string name, string position, int level, string dateRegister)
{
	_username = username;
	_password = password;
	_name = name;
	_position = position;
	_level = level;
	_dateRegister = dateRegister;
}