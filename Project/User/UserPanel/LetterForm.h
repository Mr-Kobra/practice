#pragma once
#include <ctime>
#include "../../DataBase/DataBase.h"
#include "../../Mail/Letter.h"
#include "../../Global/Global.h"
#include "../../Auth/Auth.h"

namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� LetterForm
	/// </summary>
	public ref class LetterForm : public System::Windows::Forms::Form
	{
	public:
		LetterForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}
		/*LetterForm(unsigned long int letterID) {
			InitializeComponent();
			m_letterID = letterID;
			
		}*/
		LetterForm(unsigned long int user2ID) {
			InitializeComponent();
			
			m_user2ID = user2ID;
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~LetterForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Label^ authorLabel;
	private: System::Windows::Forms::Label^ dateSendLabel;
	private: System::Windows::Forms::Label^ dateSendValue;
	private: System::Windows::Forms::Label^ messageLabel;
	private: System::Windows::Forms::RichTextBox^ messageValue;
	private: unsigned long int m_letterID;
		   unsigned long int  m_user1ID;  //��� ����������
		   unsigned long int  m_user2ID;  //��� �������� 
	private: System::Windows::Forms::Button^ SendButton;

	private: System::Windows::Forms::Label^ theme1;
	private: System::Windows::Forms::RichTextBox^ richTextBox1;
	private: System::Windows::Forms::DateTimePicker^ dateTimePicker1;

	protected:


	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->authorLabel = (gcnew System::Windows::Forms::Label());
			this->dateSendLabel = (gcnew System::Windows::Forms::Label());
			this->dateSendValue = (gcnew System::Windows::Forms::Label());
			this->messageLabel = (gcnew System::Windows::Forms::Label());
			this->messageValue = (gcnew System::Windows::Forms::RichTextBox());
			this->SendButton = (gcnew System::Windows::Forms::Button());
			this->theme1 = (gcnew System::Windows::Forms::Label());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->dateTimePicker1 = (gcnew System::Windows::Forms::DateTimePicker());
			this->SuspendLayout();
			// 
			// authorLabel
			// 
			this->authorLabel->AutoSize = true;
			this->authorLabel->Location = System::Drawing::Point(62, 23);
			this->authorLabel->Name = L"authorLabel";
			this->authorLabel->Size = System::Drawing::Size(0, 20);
			this->authorLabel->TabIndex = 1;
			// 
			// dateSendLabel
			// 
			this->dateSendLabel->AutoSize = true;
			this->dateSendLabel->Location = System::Drawing::Point(27, 640);
			this->dateSendLabel->Name = L"dateSendLabel";
			this->dateSendLabel->Size = System::Drawing::Size(146, 20);
			this->dateSendLabel->TabIndex = 2;
			this->dateSendLabel->Text = L"���� ��������: ";
			// 
			// dateSendValue
			// 
			this->dateSendValue->AutoSize = true;
			this->dateSendValue->Location = System::Drawing::Point(137, 50);
			this->dateSendValue->Name = L"dateSendValue";
			this->dateSendValue->Size = System::Drawing::Size(0, 20);
			this->dateSendValue->TabIndex = 3;
			// 
			// messageLabel
			// 
			this->messageLabel->AutoSize = true;
			this->messageLabel->Location = System::Drawing::Point(26, 151);
			this->messageLabel->Name = L"messageLabel";
			this->messageLabel->Size = System::Drawing::Size(110, 20);
			this->messageLabel->TabIndex = 4;
			this->messageLabel->Text = L"���������:";
			// 
			// messageValue
			// 
			this->messageValue->BackColor = System::Drawing::SystemColors::ControlLightLight;
			this->messageValue->Location = System::Drawing::Point(30, 191);
			this->messageValue->Name = L"messageValue";
			this->messageValue->Size = System::Drawing::Size(668, 354);
			this->messageValue->TabIndex = 5;
			this->messageValue->Text = L"";
			// 
			// SendButton
			// 
			this->SendButton->Location = System::Drawing::Point(669, 640);
			this->SendButton->Name = L"SendButton";
			this->SendButton->Size = System::Drawing::Size(116, 39);
			this->SendButton->TabIndex = 6;
			this->SendButton->Text = L"���������";
			this->SendButton->UseVisualStyleBackColor = true;
			this->SendButton->Click += gcnew System::EventHandler(this, &LetterForm::SendButton_Click);
			// 
			// theme1
			// 
			this->theme1->AutoSize = true;
			this->theme1->Location = System::Drawing::Point(26, 34);
			this->theme1->Name = L"theme1";
			this->theme1->Size = System::Drawing::Size(56, 20);
			this->theme1->TabIndex = 8;
			this->theme1->Text = L"����:";
			// 
			// richTextBox1
			// 
			this->richTextBox1->BackColor = System::Drawing::SystemColors::ControlLightLight;
			this->richTextBox1->Location = System::Drawing::Point(31, 73);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::None;
			this->richTextBox1->Size = System::Drawing::Size(667, 32);
			this->richTextBox1->TabIndex = 9;
			this->richTextBox1->Text = L"";
			// 
			// dateTimePicker1
			// 
			this->dateTimePicker1->CustomFormat = L"yyyy-MM-dd";
			this->dateTimePicker1->Enabled = false;
			this->dateTimePicker1->Location = System::Drawing::Point(179, 640);
			this->dateTimePicker1->Name = L"dateTimePicker1";
			this->dateTimePicker1->Size = System::Drawing::Size(200, 26);
			this->dateTimePicker1->TabIndex = 10;
			this->dateTimePicker1->Value = System::DateTime(2021, 7, 9, 0, 0, 0, 0);
			// 
			// LetterForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(797, 691);
			this->Controls->Add(this->dateTimePicker1);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->theme1);
			this->Controls->Add(this->SendButton);
			this->Controls->Add(this->messageValue);
			this->Controls->Add(this->messageLabel);
			this->Controls->Add(this->dateSendValue);
			this->Controls->Add(this->dateSendLabel);
			this->Controls->Add(this->authorLabel);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->Name = L"LetterForm";
			this->Text = L"LetterForm";
			this->Load += gcnew System::EventHandler(this, &LetterForm::LetterForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void LetterForm_Load(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void SendButton_Click(System::Object^ sender, System::EventArgs^ e) {
		m_user1ID = authorization->GetUserID();
		DataBase<Letter> letters(letterFile);
		DataBase<User> users(userFile);
		Letter* letter = letters.Find("ID", m_letterID);
		User* fromUser = users.Find("ID", m_user1ID);
		User* forUser = users.Find("ID", m_user2ID);
		/*letter->GetData<unsigned long int>("from user ID")*/
		string theme{}, message{}, notifyMessage1{},senDdata{};
		System::String^ notifyType1 = gcnew String("�������");
		try {
			theme = convertSysStringToChar(this->richTextBox1->Text);
			if (theme.empty()) {
				throw string("������� ����");
			}
			message = convertSysStringToChar(this->messageValue->Text);
			if (message.empty()) {
				throw string("������ ���������");
			}
		}
		catch (string str) {
			notifyMessage1 = str;
			notifyType1 = "������";
		}
		
		
		/*time_t now = time(0);
		char* dt = ctime(&now);
		cout << "The local date and time is: " << dt << endl;
		// convert now to tm struct for UTC
		tm* gmtm = gmtime(&now);
		dt = asctime(gmtm);*/
		this->dateTimePicker1->CustomFormat = L"yyyy-MM-dd";
		this->dateTimePicker1->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
		senDdata = convertSysStringToChar(this->dateTimePicker1->Text);
		Letter letter1(m_user1ID, m_user2ID, theme, message, senDdata);
		letters.Add(letter1);
		letters.Save();
		notifyMessage1 = "������ ������� ����������";
		MessageBox::Show(gcnew System::String(notifyMessage1.c_str()), notifyType1, MessageBoxButtons::OK);
		this->Close();
		/*authorLabel->Text = gcnew System::String(fromUser->GetData<string>("name").c_str());
		dateSendValue->Text = gcnew System::String(letter->GetData<string>("send date").c_str());
		messageValue->Text = gcnew System::String(letter->GetData<string>("body").c_str());*/
	}
};
}
