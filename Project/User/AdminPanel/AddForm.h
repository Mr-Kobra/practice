#pragma once
#include <vector>
#include <string>
#include "../User.h"
#include "../../Global/Global.h"
#include "../../DataBase/DataBase.h"
using namespace std;
namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^ usernameField;
	protected:
	private: System::Windows::Forms::Label^ usernameLabel;
	private: System::Windows::Forms::TextBox^ positionField;
	private: System::Windows::Forms::Label^ positionLabel;
	private: System::Windows::Forms::Label^ levelLabel;
	private: System::Windows::Forms::TextBox^ passField;
	private: System::Windows::Forms::Label^ passLabel;
	private: System::Windows::Forms::TextBox^ nameField;
	private: System::Windows::Forms::Label^ nameLabel;
	private: System::Windows::Forms::Button^ Addbutton;


	private: System::Windows::Forms::ComboBox^ levelUser;

	private: System::Windows::Forms::TextBox^ registerData;

	private: System::Windows::Forms::Label^ label2;




	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->usernameField = (gcnew System::Windows::Forms::TextBox());
			this->usernameLabel = (gcnew System::Windows::Forms::Label());
			this->positionField = (gcnew System::Windows::Forms::TextBox());
			this->positionLabel = (gcnew System::Windows::Forms::Label());
			this->levelLabel = (gcnew System::Windows::Forms::Label());
			this->passField = (gcnew System::Windows::Forms::TextBox());
			this->passLabel = (gcnew System::Windows::Forms::Label());
			this->nameField = (gcnew System::Windows::Forms::TextBox());
			this->nameLabel = (gcnew System::Windows::Forms::Label());
			this->Addbutton = (gcnew System::Windows::Forms::Button());
			this->levelUser = (gcnew System::Windows::Forms::ComboBox());
			this->registerData = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// usernameField
			// 
			this->usernameField->Location = System::Drawing::Point(44, 61);
			this->usernameField->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->usernameField->Name = L"usernameField";
			this->usernameField->Size = System::Drawing::Size(166, 20);
			this->usernameField->TabIndex = 15;
			// 
			// usernameLabel
			// 
			this->usernameLabel->AutoSize = true;
			this->usernameLabel->Location = System::Drawing::Point(42, 40);
			this->usernameLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->usernameLabel->Name = L"usernameLabel";
			this->usernameLabel->Size = System::Drawing::Size(103, 13);
			this->usernameLabel->TabIndex = 16;
			this->usernameLabel->Text = L"��� ������������";
			// 
			// positionField
			// 
			this->positionField->Location = System::Drawing::Point(44, 117);
			this->positionField->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->positionField->Name = L"positionField";
			this->positionField->Size = System::Drawing::Size(166, 20);
			this->positionField->TabIndex = 17;
			// 
			// positionLabel
			// 
			this->positionLabel->AutoSize = true;
			this->positionLabel->Location = System::Drawing::Point(42, 96);
			this->positionLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->positionLabel->Name = L"positionLabel";
			this->positionLabel->Size = System::Drawing::Size(65, 13);
			this->positionLabel->TabIndex = 18;
			this->positionLabel->Text = L"���������";
			// 
			// levelLabel
			// 
			this->levelLabel->AutoSize = true;
			this->levelLabel->Location = System::Drawing::Point(42, 156);
			this->levelLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->levelLabel->Name = L"levelLabel";
			this->levelLabel->Size = System::Drawing::Size(94, 13);
			this->levelLabel->TabIndex = 20;
			this->levelLabel->Text = L"������� �������";
			// 
			// passField
			// 
			this->passField->Location = System::Drawing::Point(248, 61);
			this->passField->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->passField->Name = L"passField";
			this->passField->Size = System::Drawing::Size(152, 20);
			this->passField->TabIndex = 21;
			// 
			// passLabel
			// 
			this->passLabel->AutoSize = true;
			this->passLabel->Location = System::Drawing::Point(246, 40);
			this->passLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->passLabel->Name = L"passLabel";
			this->passLabel->Size = System::Drawing::Size(45, 13);
			this->passLabel->TabIndex = 22;
			this->passLabel->Text = L"������";
			// 
			// nameField
			// 
			this->nameField->Location = System::Drawing::Point(248, 113);
			this->nameField->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->nameField->Name = L"nameField";
			this->nameField->Size = System::Drawing::Size(152, 20);
			this->nameField->TabIndex = 23;
			// 
			// nameLabel
			// 
			this->nameLabel->AutoSize = true;
			this->nameLabel->Location = System::Drawing::Point(246, 96);
			this->nameLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->nameLabel->Name = L"nameLabel";
			this->nameLabel->Size = System::Drawing::Size(34, 13);
			this->nameLabel->TabIndex = 24;
			this->nameLabel->Text = L"���";
			// 
			// Addbutton
			// 
			this->Addbutton->Location = System::Drawing::Point(180, 228);
			this->Addbutton->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->Addbutton->Name = L"Addbutton";
			this->Addbutton->Size = System::Drawing::Size(84, 30);
			this->Addbutton->TabIndex = 25;
			this->Addbutton->Text = L"��������";
			this->Addbutton->UseVisualStyleBackColor = true;
			this->Addbutton->Click += gcnew System::EventHandler(this, &MyForm::Addbutton_Click);
			// 
			// levelUser
			// 
			this->levelUser->Cursor = System::Windows::Forms::Cursors::Hand;
			this->levelUser->FormattingEnabled = true;
			this->levelUser->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"0", L"1" });
			this->levelUser->Location = System::Drawing::Point(44, 180);
			this->levelUser->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->levelUser->Name = L"levelUser";
			this->levelUser->Size = System::Drawing::Size(166, 21);
			this->levelUser->TabIndex = 27;
			this->levelUser->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::comboBox1_SelectedIndexChanged);
			// 
			// registerData
			// 
			this->registerData->Location = System::Drawing::Point(248, 181);
			this->registerData->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
			this->registerData->Name = L"registerData";
			this->registerData->Size = System::Drawing::Size(152, 20);
			this->registerData->TabIndex = 29;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(246, 156);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(103, 13);
			this->label2->TabIndex = 30;
			this->label2->Text = L"���� ����������� ";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(451, 295);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->registerData);
			this->Controls->Add(this->levelUser);
			this->Controls->Add(this->Addbutton);
			this->Controls->Add(this->nameLabel);
			this->Controls->Add(this->nameField);
			this->Controls->Add(this->passLabel);
			this->Controls->Add(this->passField);
			this->Controls->Add(this->levelLabel);
			this->Controls->Add(this->positionLabel);
			this->Controls->Add(this->positionField);
			this->Controls->Add(this->usernameLabel);
			this->Controls->Add(this->usernameField);
			this->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->Name = L"MyForm";
			this->Text = L"�������� ������������";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void MyForm_Load(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void Addbutton_Click(System::Object^ sender, System::EventArgs^ e) {
		string username{}, password{}, name{}, post{}, dateRegister{}, notifyMessage{};
		System::String^ notifyType = gcnew String("�������");
		int userLevel{};
		DataBase<User> Users1(userFile);
		try {
			username = convertSysStringToChar(this->usernameField->Text);
			if (username.empty()) {
				throw string("������� ��� ������������");
			}
			if (Users1.Find("username", username) != nullptr) {
				throw string("������������ � ����� ������� ��� ���������������");
			}
			password = convertSysStringToChar(this->passField->Text);
			if (password.empty()) {
				throw string("������� ������");
			}
			name = convertSysStringToChar(this->nameField->Text);
			if (name.empty()) {
				throw string("������� ���");
			}
			post = convertSysStringToChar(this->positionField->Text);
			if (post.empty()) {
				throw string("������� ���������");
			}
			userLevel = stoi(convertSysStringToChar(this->levelUser->Text));
			if (userLevel != 0 && userLevel != 1) {
				throw string("�������� ������ ������� ������ ���� 0 ��� 1");
			}
			dateRegister = convertSysStringToChar(this->registerData->Text);
			if (dateRegister.empty()) {
				throw string("������� ���� �����������");
			}
			vector<string> dateSplite = Splite(dateRegister, '-');
			if (dateSplite.size() != 3) {
				throw string("������� ���� ����������� � ������� YYYY-MM-DD");
			}
			else if (dateSplite[0].size() != 4 || dateSplite[1].size() != 2 || dateSplite[2].size() != 2) {
				throw string("������� ���� ����������� � ������� YYYY-MM-DD");
			}
			for (string date : dateSplite) {
				for (char symbol : date) {
					if ((int)symbol < 48 || (int)symbol > 57) {
						throw string("� ���� ����������� ����� ���� ������ �����");
						break;
					}
				}
			}
			User User1(username, password, name, post, dateRegister);
			User1.SetData<int>("level", userLevel);
			Users1.Add(User1);
			Users1.Save();
			notifyMessage = "������������ ������� ��������";
			this->usernameField->Clear();
			this->passField->Clear();
			this->nameField->Clear();
			this->positionField->Clear();
			this->levelUser->Text = "";
			this->registerData->Clear();
		}
		catch (string str) {
			notifyMessage = str;
			notifyType = "������";
		}
		catch (invalid_argument) {
			notifyMessage = "� ������ ������� ������ ���� ������� �����";
			notifyType = "������";
		}
		MessageBox::Show(gcnew System::String(notifyMessage.c_str()), notifyType, MessageBoxButtons::OK);
	}
	};
}
