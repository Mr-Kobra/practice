#pragma once
#include "../../Global/Global.h"
#include "../../DataBase/DataBase.h"
#include "../../Mail/Letter.h"

namespace Project {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� SendMailForm
	/// </summary>
	public ref class SendMailForm : public System::Windows::Forms::Form
	{
	public:
		SendMailForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}
		SendMailForm(unsigned long int fromUserID)
		{
			InitializeComponent();
			m_fromUserID = fromUserID;
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~SendMailForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::TextBox^ titleInput;
	private: System::Windows::Forms::RichTextBox^ messageInput;

	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: unsigned long int m_fromUserID;
	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->titleInput = (gcnew System::Windows::Forms::TextBox());
			this->messageInput = (gcnew System::Windows::Forms::RichTextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(142, 326);
			this->button1->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(112, 35);
			this->button1->TabIndex = 0;
			this->button1->Text = L"���������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &SendMailForm::button1_Click);
			// 
			// titleInput
			// 
			this->titleInput->Location = System::Drawing::Point(42, 42);
			this->titleInput->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
			this->titleInput->Name = L"titleInput";
			this->titleInput->Size = System::Drawing::Size(304, 26);
			this->titleInput->TabIndex = 1;
			// 
			// messageInput
			// 
			this->messageInput->Location = System::Drawing::Point(42, 135);
			this->messageInput->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
			this->messageInput->Name = L"messageInput";
			this->messageInput->Size = System::Drawing::Size(307, 146);
			this->messageInput->TabIndex = 2;
			this->messageInput->Text = L"";
			this->messageInput->TextChanged += gcnew System::EventHandler(this, &SendMailForm::richTextBox1_TextChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(122, 14);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(149, 20);
			this->label1->TabIndex = 3;
			this->label1->Text = L"��������� ������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(123, 111);
			this->label2->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(140, 20);
			this->label2->TabIndex = 4;
			this->label2->Text = L"����� ���������";
			this->label2->Click += gcnew System::EventHandler(this, &SendMailForm::label2_Click);
			// 
			// SendMailForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(390, 402);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->messageInput);
			this->Controls->Add(this->titleInput);
			this->Controls->Add(this->button1);
			this->Margin = System::Windows::Forms::Padding(4, 5, 4, 5);
			this->Name = L"SendMailForm";
			this->Text = L"��������� ������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void richTextBox1_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	}
private: System::Void label2_Click(System::Object^ sender, System::EventArgs^ e) {
}
private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
	string title{}, body{}, notify{}, notifyType{"�������"};
	try {
		title = convertSysStringToChar(this->titleInput->Text);
		if (title.empty()) {
			throw string("������� ��������� ������");
		}
		body = convertSysStringToChar(this->messageInput->Text);
		if (body.empty()) {
			throw string("������� ���������");
		}
		Letter letter(authorization->GetUserID(), m_fromUserID, title, body, "2021-09-07");
		DataBase<Letter> letters(letterFile);
		letters.Add(letter);
		letters.Save();
		notify = "������ ������� ����������";
	}
	catch (string str) {
		notify = str;
		notifyType = "������";
	}
	MessageBox::Show(gcnew System::String(notify.c_str()), gcnew System::String(notifyType.c_str()), MessageBoxButtons::OK);
}
};
}
