#include "Global/Global.h"
#include "Windows.h"
#include "Auth/AuthForm.h"
#include "User/AdminPanel/AdminPanel.h"
#include "User/UserPanel/UserPanel.h"
#include "Mail/Letter.h"

using namespace System;
using namespace System::Windows::Forms;

int main()
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	State::AuthForm authForm;
	Application::Run(% authForm);
	if (authorization->GetAuth()) {
		unsigned long int userID = authorization->GetUserID();
		DataBase<User>* users = new DataBase<User>(userFile);
		int level = users->Find("ID", userID)->GetData<int>("level");
		delete users;
		if (level == 1) {
			State::AdminPanelForm adminPanel;
			Application::Run(% adminPanel);
		}
		else {
			State::UserPanel userPanel;
			Application::Run(% userPanel);
		}
	} 
	return 0;
}