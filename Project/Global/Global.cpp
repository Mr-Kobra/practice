#include "Global.h"
using namespace std;

extern Auth* authorization = new Auth();

extern string userFile = "DataBase/Files/Users.txt";

extern string letterFile = "DataBase/Files/Letters.txt";

vector<string> Splite(string str, const char splite)
{
	vector<string> result{};
	string temp{};
	for (size_t i = 0; i < str.size(); i++)
	{
		if (str[i] == splite) {
			result.push_back(temp);
			temp.erase();
		}
		else if (i + 1 == str.size()) {
			temp += str[i];
			result.push_back(temp);
			temp.erase();
		}
		else {
			temp += str[i];
		}
	}
	return result;
}

char* convertSysStringToChar(System::String^ string)
{
	return (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(string);
}

bool SearchCompare(string item, string searchRequest)
{
	bool result = true;
	if (!item.empty() && !searchRequest.empty()) {
		if (searchRequest.size() < item.size()) {
			for (size_t i = 0; i < searchRequest.size(); i++)
			{
				if (searchRequest[i] != item[i]) {
					result = false;
				}
			}
		}
		else {
			result = false;
		}
	}
	else {
		result = false;
	}
	return result;
}