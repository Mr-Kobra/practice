#include "DataBase.h"
#include <fstream>
#include "../User/User.h"
#include "../Mail/Letter.h"
#include "../Global/Global.h"
using namespace std;

template <class T>
void DataBase<T>::InitBase()
{
	_items = nullptr;
	_end = nullptr;
	_len = 0;
	_filePath = string();
}
template void DataBase<User>::InitBase();
template void DataBase<Letter>::InitBase();

template <class T>
DataBase<T>::DataBase(string filePath)
{
	InitBase();
	_filePath = filePath;
	Upload();
}
template DataBase<User>::DataBase(string filePath);
template DataBase<Letter>::DataBase(string filePath);

template <class T>
DataBase<T>::DataBase(const DataBase& base)
{
	InitBase();
	_items = base._items;
	_len = base._len;
	_end = _items + 1;
	_tableItemsID = vector<unsigned long int>{};
}
template DataBase<User>::DataBase(const DataBase& base);
template DataBase<Letter>::DataBase(const DataBase& base);

template <class T>
DataBase<T>::~DataBase()
{
	cout << _len << endl;
	if (_items) {
		delete[] _items;
	}
}
template DataBase<User>::~DataBase();
template DataBase<Letter>::~DataBase();

template <class T>
void DataBase<T>::Add(T& item)
{
	if (_items == nullptr) {
		_items = new T[++_len]{};
		_items[0] = item;
		_end = _items + 1;
	}
	else {
		T* temp = new T[++_len]{};
		for (size_t i = 0; i < _len - 1; i++)
		{
			temp[i] = _items[i];
		}
		temp[_len - 1] = item;
		delete[] _items;
		_items = temp;
		_end = _items + 1;
	}
}
template void DataBase<User>::Add(User& item);
template void DataBase<Letter>::Add(Letter& item);

template <class T>
size_t DataBase<T>::Size()
{
	return _len;
}
template size_t DataBase<User>::Size();

template <class T>
T* DataBase<T>::operator[](size_t index)
{
	return &_items[index];
}
template User* DataBase<User>::operator[](size_t index);
template Letter* DataBase<Letter>::operator[](size_t index);

template <class T>
void DataBase<T>::Clear()
{
	_items = nullptr;
	_end = nullptr;
	_len = 0;
}
template void DataBase<User>::Clear();
template void DataBase<Letter>::Clear();

template <>
User* DataBase<User>::Find(string fieldName, string metaData)
{
	User* user = nullptr;
	if (fieldName == "username") {
		for (size_t i = 0; i < _len; i++)
		{
			if (_items[i].GetData<string>(fieldName) == metaData) {
				user = &_items[i];
				break;
			}
		}
	}
	return user;
}

template <class T>
T* DataBase<T>::Find(string fieldName, unsigned long int metaData)
{
	T* result = nullptr;
	if (fieldName == "ID") {
		for (size_t i = 0; i < _len; i++)
		{
			if (_items[i].GetData<unsigned long int>(fieldName) == metaData) {
				result = &_items[i];
				break;
			}
		}
	}
	return result;
}
template User* DataBase<User>::Find(string fieldName, unsigned long int metaData);
template Letter* DataBase<Letter>::Find(string fieldName, unsigned long int metaData);

template <class T>
void DataBase<T>::Save()
{
	int count = 1;
	for (size_t i = 0; i < _len; i++)
	{
		cout << _items[i].GetDataString() << endl;
		SaveString(_items[i].GetDataString(), count);
		count++;
	}
}
template void DataBase<User>::Save();
template void DataBase<Letter>::Save();

template <class T>
void DataBase<T>::SaveString(string str, int position)
{
	ofstream out;
	if (position == 1) {
		out.open(_filePath);
	}
	else {
		out.open(_filePath, ios::app);
	}
	if (out.is_open()) {
		out << str;
		if (position != Size()) {
			out << endl;
		}
	}
	out.close();
}
template void DataBase<User>::SaveString(string str, int position);
template void DataBase<Letter>::SaveString(string str, int position);

template <class T>
void DataBase<T>::Upload()
{
	this->Clear();
	ifstream in;
	in.open(_filePath);
	if (in.is_open()) {
		while (!in.eof()) {
			string str{};
			getline(in, str);
			T item(str);
			Add(item);
		}
	}
	in.close();
}
template void DataBase<User>::Upload();
template void DataBase<Letter>::Upload();

template <class T>
unsigned long int DataBase<T>::GetMaxID()
{
	unsigned long int ID = 0;
	if (Size() != 0) {
		for (size_t i = 0; i < Size(); i++)
		{
			if (_items[i].GetData<unsigned long int>(string("ID")) > ID) {
				ID = _items[i].GetData<unsigned long int>(string("ID"));
			}
		}
	}
	return ID;
}
template unsigned long int DataBase<User>::GetMaxID();
template unsigned long int DataBase<Letter>::GetMaxID();

template <>
void DataBase<User>::Print(System::Windows::Forms::ListBox^ listBox, string search)
{
	listBox->Items->Clear();
	_tableItemsID.clear();
	if (search.empty()) {
		for (size_t i = 0; i < _len; i++) {
			System::String^ name = gcnew System::String(_items[i].GetData<string>(string("name")).c_str());
			listBox->Items->Insert(i, name);
			_tableItemsID.push_back(_items[i].GetData<unsigned long int>("ID"));
		}
	}
	else {
		int j = 0;
		for (size_t i = 0; i < _len; i++)
		{
			string name = _items[i].GetData<string>(string("name"));
			if (SearchCompare(name, search)) {
				listBox->Items->Insert(j, gcnew System::String(name.c_str()));
				_tableItemsID.push_back(_items[i].GetData<unsigned long int>("ID"));
				j++;
			}
		}
	}
}

template <>
void DataBase<Letter>::Print(System::Windows::Forms::ListBox^ listBox, string search)
{
	listBox->Items->Clear();
	_tableItemsID.clear();
	if (search.empty()) {
		for (size_t i = 0; i < _len; i++) {
			if (_items[i].GetData<unsigned long int>("user ID") == authorization->GetUserID()) {
				System::String^ title = gcnew System::String(_items[i].GetData<string>(string("title")).c_str());
				listBox->Items->Insert(i, title);
				_tableItemsID.push_back(_items[i].GetData<unsigned long int>("ID"));
			}
		}
	}
	else {
		int j = 0;
		for (size_t i = 0; i < _len; i++)
		{
			string title = _items[i].GetData<string>(string("title"));
			if (SearchCompare(title, search)) {
				listBox->Items->Insert(j, gcnew System::String(title.c_str()));
				_tableItemsID.push_back(_items[i].GetData<unsigned long int>("ID"));
				j++;
			}
		}
	}
}

template <class T>
vector<unsigned long int> DataBase<T>::GetTableItems()
{
	return _tableItemsID;
}
template vector<unsigned long int> DataBase<User>::GetTableItems();
template vector<unsigned long int> DataBase<Letter>::GetTableItems();