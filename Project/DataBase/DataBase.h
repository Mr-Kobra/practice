#pragma once
#include <iostream>
#include <vector>
using namespace std;

template <class T>
class DataBase {
public:
	class DataBaseIterator {
		friend class DataBase;
	public:
		DataBaseIterator(const DataBaseIterator& it) : _item(it._item) {}
		bool operator== (const DataBaseIterator& it) const {
			return _item == it._item;
		}
		bool operator!= (const DataBaseIterator& it) const {
			return _item != it._item;
		}
		DataBaseIterator& operator++ () {
			++_item;
			return *this;
		}
		T& operator* () {
			return *_item;
		}
	private:
		T* _item;
		DataBaseIterator(T* item) : _item(item) {}
	};
	DataBase(string filePath);
	DataBase(const DataBase& base);
	~DataBase();
	void Add(T& item);
	size_t Size();
	T* operator[](size_t index);
	T* Find(string fieldName, string metaData);
	T* Find(string fieldName, unsigned long int metaData);
	void Clear();
	void Upload();
	void Save();
	void SaveString(string str, int position);
	unsigned long int GetMaxID();
	void Print(System::Windows::Forms::ListBox^ listBox, string search = string());
	vector<unsigned long int> GetTableItems();
	DataBaseIterator begin() {
		return DataBaseIterator(_items);
	}
	DataBaseIterator end() {
		return DataBaseIterator(_end);
	}
	DataBaseIterator begin() const {
		return DataBaseIterator(_items);
	}
	DataBaseIterator end() const {
		return DataBaseIterator(_end);
	}
private:
	T* _items;
	T* _end;
	size_t _len;
	string _filePath;
	vector <unsigned long int> _tableItemsID;
	void InitBase();
};