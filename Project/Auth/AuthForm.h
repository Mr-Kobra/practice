#pragma once
#include "../Global/Global.h"
#include <string>

namespace State {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� AuthForm
	/// </summary>
	public ref class AuthForm : public System::Windows::Forms::Form
	{
	public:
		AuthForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~AuthForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ loginButton;
	protected:

	protected:

	protected:


	private: System::Windows::Forms::Label^ usernameLabel;
	private: System::Windows::Forms::TextBox^ usernameField;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ passField;
	private: System::Windows::Forms::Label^ noticeLabel;

	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->loginButton = (gcnew System::Windows::Forms::Button());
			this->usernameLabel = (gcnew System::Windows::Forms::Label());
			this->usernameField = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->passField = (gcnew System::Windows::Forms::TextBox());
			this->noticeLabel = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// loginButton
			// 
			this->loginButton->Location = System::Drawing::Point(25, 144);
			this->loginButton->Margin = System::Windows::Forms::Padding(5, 7, 5, 7);
			this->loginButton->Name = L"loginButton";
			this->loginButton->Size = System::Drawing::Size(205, 31);
			this->loginButton->TabIndex = 1;
			this->loginButton->Text = L"�����";
			this->loginButton->UseVisualStyleBackColor = true;
			this->loginButton->Click += gcnew System::EventHandler(this, &AuthForm::loginButton_Click);
			// 
			// usernameLabel
			// 
			this->usernameLabel->AutoSize = true;
			this->usernameLabel->Location = System::Drawing::Point(21, 32);
			this->usernameLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->usernameLabel->Name = L"usernameLabel";
			this->usernameLabel->Size = System::Drawing::Size(166, 20);
			this->usernameLabel->TabIndex = 2;
			this->usernameLabel->Text = L"��� ������������";
			// 
			// usernameField
			// 
			this->usernameField->Location = System::Drawing::Point(25, 54);
			this->usernameField->Margin = System::Windows::Forms::Padding(2);
			this->usernameField->Name = L"usernameField";
			this->usernameField->Size = System::Drawing::Size(205, 26);
			this->usernameField->TabIndex = 3;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(21, 85);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(72, 20);
			this->label2->TabIndex = 4;
			this->label2->Text = L"������";
			// 
			// passField
			// 
			this->passField->Location = System::Drawing::Point(25, 108);
			this->passField->Margin = System::Windows::Forms::Padding(2);
			this->passField->Name = L"passField";
			this->passField->Size = System::Drawing::Size(205, 26);
			this->passField->TabIndex = 5;
			this->passField->UseSystemPasswordChar = true;
			// 
			// noticeLabel
			// 
			this->noticeLabel->AutoSize = true;
			this->noticeLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->noticeLabel->Location = System::Drawing::Point(21, 188);
			this->noticeLabel->Name = L"noticeLabel";
			this->noticeLabel->Size = System::Drawing::Size(247, 20);
			this->noticeLabel->TabIndex = 6;
			this->noticeLabel->Text = L"�������� ����� ��� ������";
			this->noticeLabel->Visible = false;
			// 
			// AuthForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(10, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(252, 228);
			this->Controls->Add(this->noticeLabel);
			this->Controls->Add(this->passField);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->usernameField);
			this->Controls->Add(this->usernameLabel);
			this->Controls->Add(this->loginButton);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->MaximizeBox = false;
			this->Name = L"AuthForm";
			this->ShowIcon = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"�����������";
			this->Load += gcnew System::EventHandler(this, &AuthForm::AuthForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void loginButton_Click(System::Object^ sender, System::EventArgs^ e) {
		string username = convertSysStringToChar(this->usernameField->Text);
		string password = convertSysStringToChar(this->passField->Text);
		bool result = authorization->Login(username, password);
		if (result) {
			this->Close();
		}
		else {
			this->noticeLabel->Visible = true;
		}
	}
private: System::Void AuthForm_Load(System::Object^ sender, System::EventArgs^ e) {	
}
};
}
