#pragma once
#include "../User/User.h"
#include <string>

using namespace std;

class Auth {
public:
	Auth();
	bool GetAuth();
	unsigned long int GetUserID();
	bool Login(string username, string password);
private:
	bool _auth;
	unsigned long int _userID;
};