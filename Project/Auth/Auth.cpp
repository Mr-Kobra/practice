#include "Auth.h"
#include "../DataBase/DataBase.h"
#include "../Global/Global.h"

using namespace std;

Auth::Auth() : _auth(false), _userID(0) {}

bool Auth::GetAuth()
{
	return _auth;
}

unsigned long int Auth::GetUserID()
{
	return _userID;
}

bool Auth::Login(string username, string password)
{
	DataBase<User> users(userFile);
	User* user = users.Find(string("username"), username);
	if (user) {
		if (user->GetData<string>("password") == password) {
			_userID = user->GetData<unsigned long int>("ID");
			_auth = true;
		}
	}
	return _auth;
}